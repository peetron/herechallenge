package com.peetron.herechallenge.model.converter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.peetron.herechallenge.model.Coordinate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class CoordinateConverterTest {

    Gson mGson = null;

    @Before
    public void setUp(){
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Coordinate.class   , new CoordinateConverter());
        mGson = builder.create();
    }

    @After
    public void tearDown(){
        mGson = null;
    }

    @Test
    public void serialize() throws Exception {
        // Arrange
        Coordinate coordinate = new Coordinate(10,15);
        String expectedOutput = "[10.0,15.0]";

        // Act
        String result = mGson.toJson(coordinate, Coordinate.class);

        // Assert
        assertThat(result, equalTo(expectedOutput));
    }

    @Test
    public void deserialize() throws Exception {
        // Arrange
        String input = "[10.0,15.0]";

        // Act
        Coordinate result = mGson.fromJson(input, Coordinate.class);

        // Assert
        assertThat(result.getLatitude(), equalTo(10d));
        assertThat(result.getLongitude(), equalTo(15d));
    }
}