package com.peetron.herechallenge;

import android.app.Application;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.peetron.herechallenge.di.ApplicationComponent;
import com.peetron.herechallenge.di.ApplicationModule;
import com.peetron.herechallenge.di.DaggerApplicationComponent;
import com.peetron.herechallenge.di.HereModule;

public class ChallengeApplication extends Application {

    private final static String BASE_URL = "https://places.cit.api.here.com/";

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        try {
            ApplicationInfo applicationInfo = getPackageManager()
                    .getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = applicationInfo.metaData;

            String appId = bundle.getString("com.here.android.maps.appid", "");
            String appCode = bundle.getString("com.here.android.maps.apptoken", "");

            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .hereModule(new HereModule(BASE_URL, appId, appCode))
                    .build();

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public ApplicationComponent getChallengeComponent() {
        return mApplicationComponent;
    }
}
