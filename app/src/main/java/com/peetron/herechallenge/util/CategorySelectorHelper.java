package com.peetron.herechallenge.util;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.ArrayAdapter;

import com.here.android.mpa.search.Category;
import com.peetron.herechallenge.R;

public class CategorySelectorHelper {

    private CategorySelectorHelper() {
        // not to be instantiated
    }

    public static void openCategorySelectorDialog(Context context, final CategoryCallback callback) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        builderSingle.setTitle(R.string.category_dialog_title);

        final Category.Global[] values = Category.Global.values();

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.select_dialog_singlechoice);
        for (Category.Global value : values) {
            arrayAdapter.add(Category.globalCategory(value).getName());
        }

        builderSingle.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                callback.categorySelected(Category.globalCategory(values[which]).getName());
            }
        });
        builderSingle.show();
    }

    public interface CategoryCallback{
        void categorySelected(String name);
    }
}
