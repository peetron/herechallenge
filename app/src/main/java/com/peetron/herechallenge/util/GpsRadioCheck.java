package com.peetron.herechallenge.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import com.peetron.herechallenge.R;

public class GpsRadioCheck {

    private GpsRadioCheck() {
        // not to be instantiated
    }

    /**
     * Check to see whether the user has the GPS radio enabled.
     */
    public static boolean gpsCheck(Activity activity) {
        final LocationManager locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

        // fetching GPS status
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showGpsSettingsAlert(activity);
            return false;
        }
        return true;
    }

    /**
     * Show GPS settings to user. Exit application if user doesn't want to enable them.
     */
    private static void showGpsSettingsAlert(final Activity activity) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

        alertDialog.setTitle(activity.getString(R.string.gps_dialog_title));
        alertDialog.setMessage(activity.getString(R.string.gps_dialog_content));

        alertDialog.setPositiveButton(activity.getString(R.string.gps_dialog_settings), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                activity.startActivity(intent);
            }
        });

        alertDialog.setNegativeButton(activity.getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                activity.finish();
            }
        });

        alertDialog.show();
    }
}
