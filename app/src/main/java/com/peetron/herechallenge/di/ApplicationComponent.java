package com.peetron.herechallenge.di;

import com.peetron.herechallenge.presenter.MainActivityPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, HereModule.class})
public interface ApplicationComponent {

    void inject(MainActivityPresenter mainActivityPresenter);
}