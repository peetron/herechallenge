package com.peetron.herechallenge.di;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.peetron.herechallenge.model.converter.CoordinateConverter;
import com.peetron.herechallenge.model.Coordinate;
import com.peetron.herechallenge.net.HereService;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class HereModule {

    private final String mBaseUrl;
    private final String mAppCode;
    private final String mAppId;

    public HereModule(String baseUrl, String appId, String appCode) {
        mBaseUrl = baseUrl;
        mAppCode = appCode;
        mAppId = appId;
    }

    @Provides
    @Singleton
    public HereService provideService() {
        return provideRetrofit().create(HereService.class);
    }

    private Retrofit provideRetrofit() {

        // Inject the app id and app code with every request
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        HttpUrl url = request.url()
                                .newBuilder()
                                .addQueryParameter("pretty", "true")
                                .addQueryParameter("app_id", mAppId)
                                .addQueryParameter("app_code", mAppCode)
                                .build();
                        request = request.newBuilder().url(url).build();
                        return chain.proceed(request);
                    }
                })
                .build();


        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Coordinate.class, new CoordinateConverter())
                .create();

        return new Retrofit.Builder()
                .baseUrl(mBaseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }
}
