package com.peetron.herechallenge.net;

import com.peetron.herechallenge.model.Coordinate;
import com.peetron.herechallenge.model.SearchResultResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface HereService {

    @GET("places/v1/discover/search")
    Call<SearchResultResponse> search(@Query("q") String query, @Query("at") Coordinate location, @Query("size") int maximumReturned);

}
