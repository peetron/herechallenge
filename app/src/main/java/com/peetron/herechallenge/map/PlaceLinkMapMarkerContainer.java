package com.peetron.herechallenge.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;

import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.mapping.MapMarker;
import com.peetron.herechallenge.model.Place;
import com.peetron.herechallenge.ui.fragment.InvalidationListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Container to ease the remote image loading.
 */
public class PlaceLinkMapMarkerContainer {

    private final MapMarker mMapMarker;
    private final Context mContext;
    private final InvalidationListener mInvalidationListener;

    public PlaceLinkMapMarkerContainer(Context context, Place place, InvalidationListener invalidationListener) {
        mContext = context;
        mInvalidationListener = invalidationListener;
        mMapMarker = new MapMarker();
        GeoCoordinate coord = new GeoCoordinate(
                place.getPosition().getLatitude(),
                place.getPosition().getLongitude());
        mMapMarker.setCoordinate(coord);
        mMapMarker.setTitle(place.getTitle());

        if (place.getIconUrl() != null) {
            Picasso.with(context)
                    .load(place.getIconUrl())
                    .into(target);
        }
    }

    public MapMarker getMapMarker() {
        return mMapMarker;
    }

    public void cancelRequest() {
        Picasso.with(mContext)
                .cancelRequest(target);
    }

    private final Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            if (bitmap != null) {
                Image image = new Image();
                image.setBitmap(bitmap);
                mMapMarker.setAnchorPoint(new PointF(image.getWidth() / 2, image.getHeight() / 2));
                mMapMarker.setIcon(image);
                mInvalidationListener.invalidate();
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
        }
    };
}