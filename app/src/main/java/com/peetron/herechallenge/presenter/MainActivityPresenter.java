package com.peetron.herechallenge.presenter;

import android.content.Context;
import android.location.Location;

import com.here.android.mpa.search.Category;
import com.peetron.herechallenge.R;
import com.peetron.herechallenge.di.ApplicationComponent;
import com.peetron.herechallenge.model.Coordinate;
import com.peetron.herechallenge.model.SearchResultResponse;
import com.peetron.herechallenge.net.HereService;
import com.peetron.herechallenge.ui.fragment.SearchResultMapFragment;
import com.peetron.herechallenge.util.CategorySelectorHelper;
import com.peetron.herechallenge.util.SingleShotLocationProvider;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivityPresenter implements MainPresenter {

    private static final int MAX_SEARCH_ITEM_RESULTS = 5;
    private static final Category.Global DEFAULT_CATEGORY = Category.Global.ACCOMMODATION;

    private final Context mContext;
    private final Ui mUi;

    @Inject
    HereService mHereService;

    private String mCurrentSearch = "";
    private SearchResultMapFragment mSearchResultMapFragment;

    public MainActivityPresenter(Context context, Ui ui, ApplicationComponent applicationComponent) {
        mContext = context;
        mUi = ui;
        applicationComponent.inject(this);
    }

    @Override
    public boolean isInitialised() {
        if (mSearchResultMapFragment == null) {
            return false;
        }
        return mSearchResultMapFragment.isMapInitialised();
    }

    @Override
    public void centreClicked() {
        centreMapOnCurrentLocation();
    }

    @Override
    public void openCategoriesClicked() {
        CategorySelectorHelper.openCategorySelectorDialog(mContext, new CategorySelectorHelper.CategoryCallback() {
            @Override
            public void categorySelected(String name) {
                MainActivityPresenter.this.categorySelected(name);
            }
        });
    }

    @Override
    public void refreshClicked() {
        mSearchResultMapFragment.setCurrentLocationAsMapCentreFocus();
        fetchPlacesForMapLocation();
    }

    @Override
    public void initialiseMapComplete(SearchResultMapFragment searchResultMapFragment) {
        mSearchResultMapFragment = searchResultMapFragment;
        Category category = Category.globalCategory(DEFAULT_CATEGORY);
        if (category != null) {
            setSearchTerm(category.getName());
        }
        centreMapOnCurrentLocation();
        mUi.updateView();
    }

    private void centreMapOnCurrentLocation() {
        SingleShotLocationProvider.requestSingleUpdate(mContext, new SingleShotLocationProvider.LocationCallback() {
            @Override
            public void onNewLocationAvailable(Location location) {
                mSearchResultMapFragment.setMapCentreFocus(location);
                fetchPlacesForMapLocation();
            }

            @Override
            public void onFailure(SecurityException securityException) {
                mUi.unrecoverableErrorDetected(mContext.getString(R.string.error_security));
            }
        });
    }

    private void fetchPlacesForMapLocation() {
        Coordinate location = new Coordinate(mSearchResultMapFragment.getMapCentreFocus().getLatitude(), mSearchResultMapFragment.getMapCentreFocus().getLongitude());
        mHereService.search(mCurrentSearch, location, MAX_SEARCH_ITEM_RESULTS).enqueue(new Callback<SearchResultResponse>() {
            @Override
            public void onResponse(Call<SearchResultResponse> call, Response<SearchResultResponse> response) {
                mSearchResultMapFragment.setSearchResults(response.body().getSearchResults().getPlaces());
            }

            @Override
            public void onFailure(Call<SearchResultResponse> call, Throwable t) {
                mUi.unrecoverableErrorDetected(mContext.getString(R.string.error_network));
            }
        });
    }

    private void setSearchTerm(String searchTerm) {
        mCurrentSearch = searchTerm;
        mUi.setTitle(searchTerm);
    }

    private void categorySelected(String name) {
        setSearchTerm(name);
        mSearchResultMapFragment.setCurrentLocationAsMapCentreFocus();
        fetchPlacesForMapLocation();
        mUi.updateView();
    }

    public interface Ui {
        void updateView();

        void setTitle(CharSequence title);

        void unrecoverableErrorDetected(String errorString);
    }
}
