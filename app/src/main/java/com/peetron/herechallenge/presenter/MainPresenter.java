package com.peetron.herechallenge.presenter;

import com.peetron.herechallenge.ui.fragment.SearchResultMapFragment;

public interface MainPresenter {

    boolean isInitialised();

    void centreClicked();

    void openCategoriesClicked();

    void refreshClicked();

    void initialiseMapComplete(SearchResultMapFragment searchResultMapFragment);
}
