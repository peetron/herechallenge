package com.peetron.herechallenge.model.converter;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.peetron.herechallenge.model.Coordinate;

import java.lang.reflect.Type;

public class CoordinateConverter implements JsonSerializer<Coordinate>, JsonDeserializer<Coordinate> {

    @Override
    public JsonElement serialize(Coordinate src, Type srcType, JsonSerializationContext context) {
        final JsonArray array = new JsonArray();
        array.add(context.serialize(src.getLatitude(), Double.class));
        array.add(context.serialize(src.getLongitude(), Double.class));
        return array;
    }

    @Override
    public Coordinate deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        final JsonArray array = json.getAsJsonArray();
        final JsonElement latitudeElement = array.get(0);
        final JsonElement longitudeElement = array.get(1);
        final Double latitude = latitudeElement.getAsDouble();
        final Double longitude = longitudeElement.getAsDouble();

        return new Coordinate(latitude, longitude);
    }
}