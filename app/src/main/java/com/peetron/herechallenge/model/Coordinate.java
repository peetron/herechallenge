package com.peetron.herechallenge.model;

public class Coordinate {

    private double mLatitude;
    private double mLongitude;

    public Coordinate(double latitude, double longitude) {
        mLatitude = latitude;
        mLongitude = longitude;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    @Override
    public String toString() {
        return mLatitude + "," + mLongitude;
    }
}
