package com.peetron.herechallenge.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchResults {

    @SerializedName("items")
    List<Place> mPlaces;

    public List<Place> getPlaces() {
        return mPlaces;
    }
}
