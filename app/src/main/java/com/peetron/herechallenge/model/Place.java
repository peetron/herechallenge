package com.peetron.herechallenge.model;

import com.google.gson.annotations.SerializedName;

public class Place {

    @SerializedName("position")
    Coordinate mPosition;

    @SerializedName("icon")
    String mIconUrl;

    @SerializedName("title")
    String mTitle;

    public String getTitle() {
        return mTitle;
    }

    public Coordinate getPosition() {
        return mPosition;
    }

    public String getIconUrl() {
        return mIconUrl;
    }
}
