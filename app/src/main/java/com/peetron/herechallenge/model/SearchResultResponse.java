package com.peetron.herechallenge.model;

import com.google.gson.annotations.SerializedName;

public class SearchResultResponse {

    @SerializedName("results")
    SearchResults mSearchResults;

    public SearchResults getSearchResults() {
        return mSearchResults;
    }
}
