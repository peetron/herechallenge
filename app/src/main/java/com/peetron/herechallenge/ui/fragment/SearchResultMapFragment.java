package com.peetron.herechallenge.ui.fragment;

import android.graphics.PointF;
import android.location.Location;
import android.view.View;

import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapFragment;
import com.here.android.mpa.mapping.MapState;
import com.peetron.herechallenge.map.PlaceLinkMapMarkerContainer;
import com.peetron.herechallenge.model.Place;

import java.util.ArrayList;
import java.util.List;

public class SearchResultMapFragment extends MapFragment implements InvalidationListener {

    private static final double DEFAULT_ZOOM_PERCENTAGE = 0.8d;
    private static final int MAX_LABELS = 5;

    private final List<PlaceLinkMapMarkerContainer> mapMarkerContainers = new ArrayList<>();
    private Map mMap;

    private GeoCoordinate mMapCentreFocus;
    private MapOverlayViewHolder mLabelHolder;
    private PointF[] mLabelPoints = new PointF[5];

    public void initialiseMap(final InitialisationCallback callback) {
        init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {
                if (error == OnEngineInitListener.Error.NONE) {
                    mapInitialisationComplete();

                    callback.onSuccess();
                } else {
                    callback.onFailure(error.toString());
                }
            }
        });
    }

    private void mapInitialisationComplete() {
        // the map is now available
        mMap = getMap();
        for (int i = 0; i < MAX_LABELS; i++) {
            mLabelPoints[i] = new PointF();
        }

        // add this for a fake overlay
        mMap.addTransformListener(new Map.OnTransformListener() {
            @Override
            public void onMapTransformStart() {
            }

            @Override
            public void onMapTransformEnd(MapState mapState) {
                updateLabels();
            }
        });
    }

    private void updateLabels() {
        for (int i = 0; i < MAX_LABELS; i++) {
            mLabelPoints[i].set(0, 0);
        }
        for (int i = 0; i < mapMarkerContainers.size(); i++) {
            PlaceLinkMapMarkerContainer container = mapMarkerContainers.get(i);
            Map.PixelResult pixelResult = mMap.projectToPixel(container.getMapMarker().getCoordinate());
            if (pixelResult.getError() == Map.PixelResult.Error.NONE) {
                mLabelPoints[i].set(pixelResult.getResult());
            }
        }
        mLabelHolder.updateLabels(mLabelPoints, mapMarkerContainers);
    }

    public void setCurrentLocationAsMapCentreFocus() {
        mMapCentreFocus = mMap.getCenter();
    }

    public void setMapCentreFocus(Location location) {
        mMapCentreFocus = new GeoCoordinate(location.getLatitude(), location.getLongitude());
        double zoomRange = mMap.getMaxZoomLevel() - mMap.getMinZoomLevel();
        mMap.setZoomLevel(mMap.getMinZoomLevel() + zoomRange * DEFAULT_ZOOM_PERCENTAGE);
        mMap.setCenter(mMapCentreFocus, Map.Animation.LINEAR);
    }

    public GeoCoordinate getMapCentreFocus() {
        return mMapCentreFocus;
    }

    public void setSearchResults(List<Place> items) {
        // clear the markers
        for (PlaceLinkMapMarkerContainer container : mapMarkerContainers) {
            mMap.removeMapObject(container.getMapMarker());
        }
        mapMarkerContainers.clear();
        for (Place item : items) {
            PlaceLinkMapMarkerContainer mapMarker = new PlaceLinkMapMarkerContainer(getActivity(), item, this);
            mapMarkerContainers.add(mapMarker);
            mMap.addMapObject(mapMarker.getMapMarker());
        }
        updateLabels();
    }

    @Override
    public void onDestroy() {
        // clear the marker targets for memory leakage
        for (PlaceLinkMapMarkerContainer container : mapMarkerContainers) {
            container.cancelRequest();
        }
        mapMarkerContainers.clear();

        super.onDestroy();
    }

    public boolean isMapInitialised() {
        return mMap != null;
    }

    public void setLabelHolder(MapOverlayViewHolder labelHolder) {
        mLabelHolder = labelHolder;
    }

    @Override
    public void invalidate() {
        View view = getView();
        if (view != null) {
            updateLabels();
            view.postInvalidate();
        }
    }

    public interface InitialisationCallback {
        void onSuccess();

        void onFailure(String errorMessage);
    }
}
