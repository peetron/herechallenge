package com.peetron.herechallenge.ui.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import com.peetron.herechallenge.ChallengeApplication;
import com.peetron.herechallenge.R;
import com.peetron.herechallenge.di.ApplicationComponent;
import com.peetron.herechallenge.presenter.MainActivityPresenter;
import com.peetron.herechallenge.ui.fragment.MapOverlayViewHolder;
import com.peetron.herechallenge.ui.fragment.SearchResultMapFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends GpsRequiredActivity implements MainActivityPresenter.Ui {

    private SearchResultMapFragment mMapFragment;

    @BindView(R.id.buttonCategories)
    FloatingActionButton mButtonCategories;
    @BindView(R.id.buttonCentre)
    FloatingActionButton mButtonCentre;
    @BindView(R.id.buttonRefresh)
    FloatingActionButton mButtonRefresh;

    private MainActivityPresenter mMainActivityPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ApplicationComponent applicationComponent = ((ChallengeApplication) getApplication()).getChallengeComponent();
        mMainActivityPresenter = new MainActivityPresenter(this, this, applicationComponent);

        setupView();

        // NOTE: this only exists because I can't add views to MapFragment
        MapOverlayViewHolder mapOverlayViewHolder = new MapOverlayViewHolder(this);
        mMapFragment = (SearchResultMapFragment) getFragmentManager().findFragmentById(R.id.mapfragment);
        mMapFragment.setLabelHolder(mapOverlayViewHolder);
    }

    //region GpsRequiredActivity
    @Override
    void onResumeWithGpsEnabledAndAllowed() {
        if (!mMapFragment.isMapInitialised()) {
            mMapFragment.initialiseMap(new SearchResultMapFragment.InitialisationCallback() {
                @Override
                public void onSuccess() {
                    mMainActivityPresenter.initialiseMapComplete(mMapFragment);
                }

                @Override
                public void onFailure(String errorMessage) {
                    unrecoverableErrorDetected(getString(R.string.error_map_fragment, errorMessage));
                }
            });
        }
    }

    @Override
    void userDeniedPermissionRequest(String errorMessage) {
        unrecoverableErrorDetected(errorMessage);
    }
    //endregion

    //region View
    private void setupView() {
        ButterKnife.bind(this);

        mButtonCategories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMainActivityPresenter.openCategoriesClicked();
            }
        });

        mButtonCentre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMainActivityPresenter.centreClicked();
            }
        });
        mButtonRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMainActivityPresenter.refreshClicked();
            }
        });
        updateView();
    }

    @Override
    public void updateView() {
        if (mMainActivityPresenter.isInitialised()) {
            mButtonCentre.setVisibility(View.VISIBLE);
            mButtonCategories.setVisibility(View.VISIBLE);
            mButtonRefresh.setVisibility(View.VISIBLE);
        } else {
            mButtonCentre.setVisibility(View.GONE);
            mButtonCategories.setVisibility(View.GONE);
            mButtonRefresh.setVisibility(View.GONE);
        }
    }

    //endregion
}