package com.peetron.herechallenge.ui.fragment;

import android.app.Activity;
import android.graphics.PointF;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.here.android.mpa.mapping.MapMarker;
import com.peetron.herechallenge.R;
import com.peetron.herechallenge.map.PlaceLinkMapMarkerContainer;

import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;

public class MapOverlayViewHolder {

    @BindViews({R.id.textViewLabel1, R.id.textViewLabel2, R.id.textViewLabel3, R.id.textViewLabel4, R.id.textViewLabel5})
    List<TextView> mTextViews;

    public MapOverlayViewHolder(Activity activity) {
        ButterKnife.bind(this, activity);
    }

    void updateLabels(PointF[] labelPoints, List<PlaceLinkMapMarkerContainer> containers) {
        for (int i = 0; i < mTextViews.size(); i++) {
            TextView textView = mTextViews.get(i);
            if (containers.size() > i) {
                MapMarker mapMarker = containers.get(i).getMapMarker();
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) textView.getLayoutParams();
                layoutParams.leftMargin = (int) (labelPoints[i].x);
                layoutParams.topMargin = (int) (labelPoints[i].y + mapMarker.getAnchorPoint().y);
                textView.setVisibility(View.VISIBLE);
                textView.setLayoutParams(layoutParams);
                textView.setText(mapMarker.getTitle());
                textView.requestLayout();
            } else {
                textView.setVisibility(View.GONE);
            }
        }
    }
}