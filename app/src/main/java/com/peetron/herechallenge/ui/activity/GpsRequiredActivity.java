package com.peetron.herechallenge.ui.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.peetron.herechallenge.R;
import com.peetron.herechallenge.util.GpsRadioCheck;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class GpsRequiredActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_ASK_PERMISSIONS = 69;
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    abstract void onResumeWithGpsEnabledAndAllowed();

    abstract void userDeniedPermissionRequest(String errorMessage);

    @Override
    protected void onResume() {
        super.onResume();

        // Check if the user actually has the gps settings enabled
        if (GpsRadioCheck.gpsCheck(this)) {
            // Now check to see if the user grants permission to use it
            // NOTE: We're also checking for the WRITE_EXTERNAL_STORAGE permission at the same time
            checkPermissions();
        }
    }

    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<>();
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            final String[] permissions = missingPermissions.toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS, grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = 0; index < permissions.length; index++) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        userDeniedPermissionRequest(String.format(getString(R.string.permission_denied), permissions[index]));
                        return;
                    }
                }
                onResumeWithGpsEnabledAndAllowed();
                break;
        }
    }

    public void unrecoverableErrorDetected(String errorString) {
        Toast.makeText(this, errorString, Toast.LENGTH_LONG).show();
        finish();
    }
}