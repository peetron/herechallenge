package com.peetron.herechallenge.ui.fragment;

public interface InvalidationListener {
    void invalidate();
}
