package com.peetron.herechallenge.presenter;

import android.content.Context;
import android.os.Looper;
import android.support.test.InstrumentationRegistry;

import com.peetron.herechallenge.di.ApplicationComponent;
import com.peetron.herechallenge.di.DaggerApplicationComponent;
import com.peetron.herechallenge.di.HereModule;
import com.peetron.herechallenge.net.HereService;
import com.peetron.herechallenge.ui.fragment.SearchResultMapFragment;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MainActivityPresenterTest {

    @Module
    public class TestHereModule extends HereModule {
        public TestHereModule(String baseUrl, String appId, String appCode) {
            super(baseUrl, appId, appCode);
        }

        @Override
        @Provides
        @Singleton
        public HereService provideService() {
            mTestHereService = Mockito.mock(HereService.class);
            return mTestHereService;
        }
    }

    private MainActivityPresenter mMainActivityPresenter;
    private MainActivityPresenter.Ui mUi;
    private HereService mTestHereService;

    @Before
    public void setUp() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        TestHereModule testHereModule = new TestHereModule(null, null, null);
        ApplicationComponent component = DaggerApplicationComponent
                .builder()
                .hereModule(testHereModule)
                .build();

        mUi = mock(MainActivityPresenter.Ui.class);
        mMainActivityPresenter = new MainActivityPresenter(appContext, mUi, component);
    }

    @After
    public void tearDown() {
        mUi = null;
        mMainActivityPresenter = null;
        mTestHereService = null;
    }

    @Test
    public void isInitialised_beforeMapSet_false() throws Exception {
        // Assert
        assertThat(mMainActivityPresenter.isInitialised(), equalTo(false));
    }

    @Test
    public void isInitialised_afterMapSet_true() throws Exception {
        // Act
        SearchResultMapFragment searchResultMapFragment = mock(SearchResultMapFragment.class);
        when(searchResultMapFragment.isMapInitialised()).thenReturn(true);
        mMainActivityPresenter.initialiseMapComplete(searchResultMapFragment);

        // Assert
        assertThat(mMainActivityPresenter.isInitialised(), equalTo(true));
    }
}